
#include <opencv2/opencv.hpp>
#include <iostream>

// -----> /dev/video2
#define usbCamLeft 2

// -----> /dev/video4
#define usbCamRight 4

using namespace cv;
using namespace std;

int main(int argc, char* argv[])
{
#if defined(__linux__)
	//Open the left camera
	VideoCapture cap_left(usbCamLeft);
	//Open the right camera
	VideoCapture cap_right(usbCamRight);
#elif defined(_WIN32) || defined(_WIN64)
        //Open the left camera
        VideoCapture cap_left(usbCamLefti + cv::CAP_DSHOW);
        //Open the right camera
        VideoCapture cap_right(usbCamRight + cv::CAP_DSHOW);
#endif

	// if not success, exit program
	if (cap_left.isOpened() == false)  
	{
		cout << "Cannot open the left camera" << endl;
		cin.get(); //wait for any key press
		return -1;
	} 

	if (cap_right.isOpened() == false)
	{
		cout << "Cannot open the right camera" << endl;
		cin.get(); //wait for any key press
		return -1;
	}

	double dWidth_left = cap_left.get(CAP_PROP_FRAME_WIDTH); //get the width of frames of the video
	double dHeight_left = cap_left.get(CAP_PROP_FRAME_HEIGHT); //get the height of frames of the video

	cout << "Resolution of the left : " << dWidth_left << " x " << dHeight_left << endl;

	double dWidth_right = cap_right.get(CAP_PROP_FRAME_WIDTH); //get the width of frames of the video
	double dHeight_right = cap_right.get(CAP_PROP_FRAME_HEIGHT); //get the height of frames of the video

	cout << "Resolution of the right : " << dWidth_right << " x " << dHeight_right << endl;


	string window_left_name = "Left eye";
	string window_right_name = "Right eye";
	namedWindow(window_left_name); //create a window called "My Camera Feed"
	namedWindow(window_right_name); //create a window called "My Camera Feed"

	while (true)
	{
		Mat frame_left, frame_right;
		bool bSuccess_left = cap_left.read(frame_left); // read a new frame from video 
		bool bSuccess_right = cap_right.read(frame_right); // read a new frame from video 

		//Breaking the while loop if the frames cannot be captured
		if (bSuccess_left == false) 
		{
			cout << "Left camera is disconnected" << endl;
			cin.get(); //Wait for any key press
			break;
		}

		//Breaking the while loop if the frames cannot be captured
		if (bSuccess_right == false)
		{
			cout << "Right camera is disconnected" << endl;
			cin.get(); //Wait for any key press
			break;
		}


		//show the frame in the left window
		imshow(window_left_name, frame_left);

		//show the frame in the right window  
		imshow(window_right_name, frame_right);

		//wait for for 10 ms until any key is pressed.  
		//If the 'Esc' key is pressed, break the while loop.
		//If the any other key is pressed, continue the loop 
		//If any key is not pressed withing 10 ms, continue the loop 
		if (waitKey(10) == 27)
		{
			cout << "Esc key is pressed by user. Stoppig the video" << endl;
			break;
		}
	}

	return 0;

}
